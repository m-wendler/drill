FROM python:3.8-slim

WORKDIR /drill

COPY requirements.txt /drill
RUN pip install -r requirements.txt

COPY src/drill/*.py /drill/src/drill/
COPY data/* /drill/data/

WORKDIR src/drill
ENTRYPOINT ["python"]
CMD ["main.py"]
