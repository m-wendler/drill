"""
A simple program challenging the user to practice a random task.
Inspired by 'spaced repetition systems' (flash cards) and may grow into one ...
"""

from cards import nextcard, contains
import ingestion
from argparse import ArgumentParser
import logging


def chose_next_challenge(cards, tags):
    """:returns the next challenge"""

    card = nextcard(cards)
    if tags:
        counter = 0
        while not contains(tags, card.tags) and counter < len(cards):
            card = nextcard(cards)
            counter += 1
        else:
            if counter == len(cards):
                print(f"no card found for tags: {tags}")
                return None

    return card


def parser():
    """:returns a configured argument parser"""

    return ArgParser()


class ArgParser():
    """wraps an ArgumentParser for higher-level access"""

    def __init__(self):
        self.argparser = ArgumentParser()
        self.argparser.add_argument('-t', '--tags', type=str,
                                    help="""choose only from challenges
                               with this tag. (currently only one tag 
                               is supported).""")
        self.args = self.argparser.parse_args()

    def __str__(self):
        return self.args.__str__()

    def tags(self):
        """:returns the desired tags or an empty list"""
        return self.args.tags.split() if self.args.tags else []


def main():
    """reads in the challenges and presents a randomly chosen one"""

    try:
        css = ingestion.read_data()
    except IndexError as e:
        logging.error("reading in the data failed", e)
    else:
        logging.debug(f"read in {len(css)} cards")
        for card in css:
            logging.debug(f"  {card}")

        p = parser()
        tags = p.tags()
        logging.debug(f"{p} / {tags}")

        card = chose_next_challenge(css, tags)
        if card:
            print('Practice today:')
            print(f"\n  {card.challenge} ({', '.join(card.tags)})")


if __name__ == '__main__':
    logging.basicConfig(filename="log.txt",
                        level=logging.DEBUG,
                        format=f"%(asctime)s - %(levelname)s - %(message)s")

    main()
