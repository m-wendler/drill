"""Simple flash cards"""

from collections import namedtuple
import random

Card = namedtuple('Card', 'id challenge tags refs')


def nextcard(cs) -> Card:
    """:returns a random card"""

    return random.choice(cs)


def contains(giventags, cardtags):
    """:returns True if the given tags are all contained in the card's tags"""
    return all([x in cardtags for x in giventags])
