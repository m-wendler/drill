"""loads the data

    :todo
    * Use a factory to convert CSV data to cards.
    * Map factories to versions to accomodate for 'schema changes'.
"""

import csv
from cards import Card
import logging

FILE_NAME = '../../data/data.csv'


def create_card(row):
    """:returns a newly created card from the list of strings"""
    return Card(int(row[0]), row[1].lstrip(), row[2].split(), row[3].lstrip())


def read_data():
    """reads the data from a CSV file

        :todo
        * more robust exception handling/error analysis
        * inject creator factory
    """

    with open(FILE_NAME) as data:
        reader = csv.reader(data)
        _ = next(reader)

        return csvdata_to_cards(reader)


def csvdata_to_cards(reader):
    """:returns the csv data converted to a list of Cards"""

    rows = []
    for row in reader:
        try:
            c = create_card(row)
        except:
            logging.error("processing this row failed:", row)
        else:
            rows.append(c)

    return rows