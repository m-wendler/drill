Drill
=====

Learning knowledge and skills and keeping your chops current can be challenging.
So how about doing a virtual workout and practice certain topics regularly?

Drill will challenge you with a randomly chosen task for you to do. *Randomly* so you also
practice your not-so-favourite things.

Simply run

    > ``main.py``

which will spit out a different challenge every time, e.g.::

    > Practice today:
    >
    >   when do you use `@staticmethod` or `@classmethod`? (OOP)


To get a challenge with a specific tag add the desired tag as a command line paramter, e.g.

    > ``main.py -t json``

Challenges
----------

Short python tasks stored in ``data/data.csv``.

