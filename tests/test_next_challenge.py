from ingestion import csvdata_to_cards
from main import chose_next_challenge

import pytest


@pytest.fixture()
def testcards(csv_test_data):
    return csvdata_to_cards(csv_test_data)


def test_happy_path(testcards):
    for tags in ([], ['json']):
        card = chose_next_challenge(testcards, tags)

        assert card


def test_no_card(testcards):
    for tags in (['bla'], ['json, functional']):
        card = chose_next_challenge(testcards, tags)

        assert not card