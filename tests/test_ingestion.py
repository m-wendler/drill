from ingestion import create_card, csvdata_to_cards
from cards import Card


def test_csv_conversion(csv_test_data):
    data = csv_test_data
    actual = csvdata_to_cards(data)

    assert len(actual) == 4


def test_card_creation():
    row = ['1', ' serialize an object to JSON', ' json', ' 2']
    expected = Card(1, 'serialize an object to JSON', ['json'], '2')
    actual = create_card(row)

    assert actual == expected
