
from cards import contains


def test_contains():
    cases = [
        [['one'], ['one']],
        [['one'], ['one', 'two', 'three']]
    ]

    for testcase in cases:
        actual = contains(testcase[0], testcase[1])

        assert actual


def test_contains_negative():
    cases = [
        [[''], ['one']],
        [['one'], ['two', 'three']],
        [['one', 'two'], ['two', 'three']],
        [['one'], []]
    ]

    for testcase in cases:
        actual = contains(testcase[0], testcase[1])

        assert not actual
