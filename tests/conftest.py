import csv

import pytest


@pytest.fixture()
def csv_test_data():
    data = """1, serialize an object to JSON, json, 2
              2, deserialize an object from JSON, json, 1
              3, define a function with a variable number of arguments, function,
              4, use `reduce()` to sum all values in a sequence, json fp,"""


    return csv.reader(data.splitlines())
