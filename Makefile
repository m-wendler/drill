build_image:
	docker build -t drill .

test:
	pytest

run:
	cd src/drill && python3 main.py && cd ../..


run_container:
	docker-compose up

